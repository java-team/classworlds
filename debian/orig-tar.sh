#!/bin/sh -e

VERSION=$2
TAR=../classworlds_$VERSION.orig.tar.gz
DIR=classworlds-$VERSION
TAG=$(echo "CLASSWORLDS_$VERSION" | sed 's,1\.1-final,1_1,' | sed 's,-\(alpha\),_ALPHA_,' | sed 's,-\(beta\),_BETA_,' | sed 's,-\(rc\),_RC_,' | sed 's,\([0-9]\)\.,\1_,g' )

svn export http://svn.codehaus.org/classworlds/tags/$TAG/classworlds/ $DIR
tar -c -z -f $TAR $DIR
rm -rf $DIR ../$TAG
